from sqlalchemy import create_engine

username = 'tornado'
password = 'tornado'
schema = 'tornado'

host = 'db'

url = f'postgresql://{username}:{password}@{host}:5432/{schema}'
engine = create_engine(url, echo=True)

