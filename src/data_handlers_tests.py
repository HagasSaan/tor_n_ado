import datetime
from unittest import mock

import pytest

from data_handlers import get_data, post_data
from db.schema import SomeTable


@pytest.fixture
def f_session() -> mock.Mock:
    return mock.Mock()


def test_get_data_without_id() -> None:
    with pytest.raises(Exception, match='ID not specified'):
        get_data(None)


def test_get_data_with_id(
    f_session: mock.Mock,
) -> None:
    actual_row = SomeTable(
        id=1,
        array=[1, 2],
        result_array=[1, 4],
        date_of_creation=datetime.date(2020, 4, 1)
    )
    f_session.query(SomeTable).get.return_value = actual_row

    expected_row = {
        'id': 1,
        'array': [1, 2],
        'result_array': [1, 4],
        'date_of_creation': '2020-04-01',
    }

    assert get_data(1, f_session) == expected_row


def test_post_data_without_array() -> None:
    with pytest.raises(Exception, match='Array not specified'):
        post_data(None)


def test_post_data_with_array(
    f_session: mock.Mock,
) -> None:
    actual_array = [1, 2]
    post_data(actual_array, f_session)

    assert isinstance(f_session.add.call_args[0][0], SomeTable)
    row = f_session.add.call_args[0][0]
    assert row.array == actual_array

    assert f_session.commit.called
