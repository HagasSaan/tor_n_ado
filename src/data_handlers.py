from typing import Optional, Any

from sqlalchemy.orm import sessionmaker

from db.schema import SomeTable
from db.settings import engine

Session = sessionmaker(bind=engine)
default_session = Session()


def get_data(
    _id: Optional[int],
    session: Session = default_session,
) -> dict[str, Any]:
    if not _id:
        raise Exception('ID not specified')

    row = session.query(SomeTable).get(_id)

    return row.as_dict()


def post_data(
    array: Optional[list],
    session: Session = default_session,
) -> dict[str, Any]:
    if not array:
        raise Exception('Array not specified')

    result_array = map(lambda x: x ** 2, array)

    row = SomeTable(
        array=array,
        result_array=result_array,
    )

    session.add(row)
    session.commit()

    return {
        'result': 'OK',
        'id': row.id,
    }
