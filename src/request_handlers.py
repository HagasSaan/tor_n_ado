import json

import tornado.web
from tornado_sqlalchemy import SessionMixin

from data_handlers import get_data, post_data


class AsyncMainHandler(tornado.web.RequestHandler, SessionMixin):
    async def get(self):
        body = self.request.body
        try:
            body = json.loads(body)
            with self.make_session() as session:
                data = get_data(_id=body.get('id'), session=session)
            self.write(json.dumps(data))
        except Exception as e:
            self.set_status(400)
            self.write(json.dumps({'result': f'ERROR: {e}'}))

    async def post(self):
        body = self.request.body
        try:
            body = json.loads(body)
            with self.make_session() as session:
                data = post_data(array=body.get('array'), session=session)
            self.write(json.dumps(data))
        except Exception as e:
            self.set_status(400)
            self.write(json.dumps({'result': f'ERROR: {e}'}))
