import json
import requests


def test_post_array_and_get_by_id() -> None:
    expected_array = [1.0, 2.0, 3.0]
    expected_result_array = list(map(lambda x: x**2, expected_array))

    response = requests.post(
        'http://localhost:8888',
        data=json.dumps({'array': expected_array}),
    )
    data = response.json()

    response = requests.get(
        'http://localhost:8888',
        data=json.dumps({'id': data['id']})
    )
    data = response.json()

    assert data['array'] == expected_array
    assert data['result_array'] == expected_result_array
