import tornado.ioloop
import tornado.web
from tornado.httpserver import HTTPServer
from tornado_sqlalchemy import SQLAlchemy

from db.settings import url as db_url
from request_handlers import AsyncMainHandler


def make_app():
    return HTTPServer(
        tornado.web.Application(
            [
                (r'/', AsyncMainHandler),
            ],
            db=SQLAlchemy(db_url),
        )
    )


if __name__ == '__main__':
    make_app().listen(8888)
    tornado.ioloop.IOLoop.instance().start()
