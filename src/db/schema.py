import datetime
from typing import Any

from sqlalchemy import Column, Integer, Date, Float
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class SomeTable(Base):
    __tablename__ = 'some_table'

    id = Column(Integer, primary_key=True)
    array = Column(ARRAY(Float))
    result_array = Column(ARRAY(Float))
    date_of_creation = Column(Date, default=datetime.datetime.today)

    def as_dict(self) -> dict[str, Any]:
        result = {}
        for column in self.__table__.columns:
            value = getattr(self, column.name)
            if isinstance(value, datetime.date):
                value = value.isoformat()

            result[column.name] = value

        return result
