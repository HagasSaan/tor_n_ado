FROM python:3.9
RUN mkdir /application
WORKDIR /application
COPY . /application/
RUN pip3 install -r requirements.txt
CMD python3 src/main.py
